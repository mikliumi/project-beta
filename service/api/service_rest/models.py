from django.db import models
from django.urls import reverse


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)    

    def get_api_url(self):
        return reverse("api_automobile", kwargs={"vin": self.vin})


class TechnicianVO(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Technician(models.Model):
    name = models.CharField(max_length=50)
    employee_number = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name
    

class Appointment(models.Model):
    customer_name = models.CharField(max_length=50)
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.TextField()
    finished = models.BooleanField(default=False, null=True)
    cancelled = models.BooleanField(default=False, null=True)

    technician = models.ForeignKey(
        Technician,
        related_name="appointment",
        on_delete=models.PROTECT,
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="appointment",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_show_appointment", kwargs={"id": self.id})
