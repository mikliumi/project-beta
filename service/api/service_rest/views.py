from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .encoders import(
    TechnicianEncoder,
    AppointmentListEncoder,
    AppointmentDetailEncoder,
)
from .models import AutomobileVO, Technician, Appointment


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.filter(finished=False, cancelled=False)
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder
        )
    else:
        content = None
        try:
            content = json.loads(request.body)
        except json.JSONDecodeError:
            return JsonResponse(
                {"message": "Invalid JSON"},
                status=400
            )
        employee_number = content["employee_number"]
        try:
            technician = Technician.objects.get(employee_number=employee_number)
            content["technician"] = technician
            del content["employee_number"]
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": f"Technician: employee_number({employee_number}) does not exist"},
                status=400,
            )
        vin = content["vin"]
        try:
            auto = AutomobileVO.objects.get(vin=vin)  
            content["automobile"] = auto
            del content["vin"]
        except AutomobileVO.DoesNotExist:
            auto = AutomobileVO.objects.create(vin=vin)    
            content["automobile"] = auto
            del content["vin"]
        appointment = None
        try:        
            appointment = Appointment.objects.create(**content)

        except Exception as e:
            return JsonResponse(
                {"message": "Opps cant create Appointment."},
                status=400
            )
        try:
            
            return JsonResponse(
                {},
                status=200
            )
        except Exception as e:
            return JsonResponse(
                {"message": "Opps cant decode Appointment."},
                status=400
            )
            
       



@require_http_methods(["GET", "PUT"])
def api_show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            if "automobile" in content:
                automobile = AutomobileVO.objects.get(id=id)
                content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Automobile Vin"},
                status=400,
            )
        Appointment.objects.filter(id=id).update(**content)
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"Technician": technicians},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False
        )

   
@require_http_methods(["GET"])
def api_show_history(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            appointments,
            encoder=AppointmentListEncoder,
            safe=False,
        )