from django.urls import path

from .views import (
    api_list_salespeople,api_list_customers,api_list_salesrecords, api_list_available_cars
)

urlpatterns = [
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("customers/", api_list_customers, name="api_list_customers"),
    path("sales/", api_list_salesrecords, name="api_list_sales_records"),
    path("available/",api_list_available_cars, name="api_lsit_available_cars")
]
