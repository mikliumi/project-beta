from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from .models import AutomobileVO, Customer, Salesperson, SalesRecord
import json
from django.http import JsonResponse
from .encoders import SalespersonEncoder, CustomerEncoder, SalesRecordEncoder


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"Salespeople": salespeople},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            salesperson = Salesperson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
            )
        except: 
            return JsonResponse(
                {"error": "Failed to create Salesperson"}, status=400
            )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"Customer": customers},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except:
            return JsonResponse(
                {"Error": "Failed to create customer"}, status=400
            )

@require_http_methods(["GET", "POST"])
def api_list_salesrecords(request, employee_id=None):
    if request.method == "GET":
        if employee_id is not None:
            sales = SalesRecord.objects.filter(id = employee_id)
        else:
            sales = SalesRecord.objects.all()
        return JsonResponse(
            {"Sales": sales},
            encoder=SalesRecordEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            sold_vin = content["automobile"]
            automobile = AutomobileVO.objects.get(vin = sold_vin)
            content["automobile"] = (automobile)

            salesperson_name = content["salesperson"]
            salesperson = Salesperson.objects.get(name = salesperson_name)
            content["salesperson"] = salesperson

            customer_name = content["customer"]
            customer = Customer.objects.get(name=customer_name)
            content["customer"] = customer
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "invalid vin number; cannot complete sale"}, status=400
            )
        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            sales_record,
            encoder = SalesRecordEncoder,
            safe=False,
        )


@require_http_methods(["GET"])
def api_list_available_cars(request):
    if request.method == "GET":
        available_cars = []
        all_cars = AutomobileVO.objects.values_list('id', 'vin')
        sold_cars = SalesRecord.objects.values_list('automobile_id', flat=True)
        for each in all_cars:
            if each[0] not in sold_cars:
                available_cars.append({'id, vin': each})
        return JsonResponse(
            {"Available": available_cars},
            encoder=SalespersonEncoder,
            safe=False,
        )
