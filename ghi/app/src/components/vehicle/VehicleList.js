import { useEffect, useState } from 'react';

function VehicleList() {
  const [models, setVehicle] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/models/');
    if (response.ok) {
      const data = await response.json();
      setVehicle(data.models)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <>

    <h1>
      Vehicle Models
    </h1>
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Manufacturer</th>
          <th>Picture</th>
        </tr>
      </thead>
      <tbody>
        {models.map(vehicle => {
          return (
            <tr key={vehicle.href}>
              <td>{ vehicle.name }</td>
              <td>{ vehicle.manufacturer.name }</td>
              <td><img src={ vehicle.picture_url } alt="Car" width="400" height="700"/></td>
            </tr>
          );
        })}
      </tbody>
    </table>
    </>
  );
}

export default VehicleList;
