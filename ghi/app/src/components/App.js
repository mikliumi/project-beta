import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AutomobileList from './automobile/AutomobileList';
import AutomobileForm from './automobile/AutomobileForm';
import ManufacturerList from './manufacturer/ManufacturerList';
import ManufacturerForm from './manufacturer/ManufacturerForm';
import VehicleList from './vehicle/VehicleList';
import VehicleForm from './vehicle/VehicleForm';
import SalespersonForm from "./salesperson/SalespersonForm";
import SalespersonSalesList from "./salesperson/SalespersonSalesList";
import CustomerForm from "./customer/CustomerForm";
import SalesList from "./sales/SalesList";
import SalesForm from "./sales/SalesForm";
import TechnicianForm from './service/TechnicianForm';
import AppointmentForm from './service/AppointmentForm';
import AppointmentList from './service/AppointmentList';
import ServiceHistory from './service/ServiceHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturer" element={<ManufacturerList />} />
          <Route path="/manufacturer_add" element={<ManufacturerForm />} />
          <Route path="/vehicle" element={<VehicleList />} />
          <Route path="/vehicle_add" element={<VehicleForm />} />
          <Route path="/automobile" element={<AutomobileList />} />
          <Route path="/automobile_add" element={<AutomobileForm />} />
          <Route path="/salesperson/add" element={<SalespersonForm />} />
          <Route path="/salesperson/sales" element={<SalespersonSalesList />} />
          <Route path="/customer/add" element={<CustomerForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales_add" element={<SalesForm />} />
          <Route path="/technician/add" element={<TechnicianForm /> } />
          <Route path="/appointment_add" element={<AppointmentForm /> } />
          <Route path="/appointment" element={<AppointmentList /> } />
          <Route path="/history" element={<ServiceHistory /> } />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
