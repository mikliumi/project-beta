import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';

function AppointmentList () {
    const [appointments, setAppointments] = useState([])

    const getData = async () => {
        const resp = await fetch(`http://localhost:8080/api/appointments/`)
        if(resp.ok) {
            const data = await resp.json()
            setAppointments(data.appointments)
        }
    }

    useEffect(()=> {
        getData()
    }, [])

    const handleButton = async (e) => {
        const appointmentId = parseInt(e.target.id);
        const buttonClicked = e.target.name;

        const url = `http://localhost:8080/api/appointments/${appointmentId}/`;

        const data = {};
        if (buttonClicked === 'cancel') {
            data['cancelled'] = true;
        } else if (buttonClicked === 'finished') {
            data['finished'] = true;
        }

        const fetchConfigs = {
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const resp = await fetch(url, fetchConfigs);
        
        if (resp.ok) {
            const updatedAppointmentList = appointments.filter(appointment => appointment.id !== appointmentId);
            setAppointments(updatedAppointmentList);
        }
    }

    return ( <>
        <div className="row">
          <div className="offset-2 col-8">
            <div className="shadow p-4 mt-4">
              <h1>Appointment List</h1>
              <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Action</th>
                    </tr>
                </thead>

                <tbody>
                    {
                        appointments.map(appointment => {
                            return (
                                <tr key={appointment.id + `${Math.random() * 1000}`}>
                                    <td>{ appointment.automobile }</td>
                                    <td>{ appointment.customer_name}</td>
                                    <td>{ appointment.date }</td>
                                    <td>{ appointment.time }</td>
                                    <td>{ appointment.technician }</td>
                                    <td>{ appointment.reason }</td>
                                    <td>
                                        <div className="btn-group" role="group" aria-label="Basic example">
                                            <button onClick={handleButton} id={appointment.id} name="cancel" className="btn btn-danger">Cancel</button>
                                            <button onClick={handleButton} id={appointment.id} name="finished" className="btn btn-success">Finished</button>
                                        </div>
                                    </td>
                                </tr>
                            );
                        })
                    }
                </tbody>
              </table>
              <Link to="/appointment/add"><button className='btn btn-primary'>Create a Service Appointment</button></Link>
            </div>
          </div>
        </div>
    </>
    );
 }
    
export default AppointmentList;