import React, {useState, useEffect} from 'react';

function TechnicianForm () {
    const [technician, setTechnician] = useState({
        name: '',
        employee_number: ''
    })

    const handleSubmit = async (event) => {
        event.preventDefault();

        const technicianUrl = `http://localhost:8080/api/technician/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(technician),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(technicianUrl, fetchConfig);
        
        if (response.ok) {
            setTechnician({
                name: '',
                employee_number: ''
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputId = e.target.id;
        setTechnician({
            ...technician,
            [inputId]: value
        });
    }

    useEffect(()=> {
      fetch(setTechnician);
  }, [])
      
    return (
        <div className="row">
          <div className="offset-2 col-8">
            <div className="shadow p-4 mt-4">
              <h1>Add a Technician</h1>
              <form onSubmit={handleSubmit} id="create-technician-form">
                <div className="form-floating mb-3">
                  <input value={technician.name} onChange={handleFormChange} placeholder="technician name" required type="text" name="name" id="name" className="form-control" />
                  <label htmlFor="name">Technician Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={technician.employee_number} onChange={handleFormChange} placeholder="employee number" required type="text" name="employee_number" id="employee_number" className="form-control" />
                  <label htmlFor="employee_number">Employee Number</label>
                </div>
                <button className="btn btn-primary">Add</button>
              </form>
            </div>
          </div>
        </div>
    );
 }
    
export default TechnicianForm;