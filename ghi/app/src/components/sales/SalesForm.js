import React, {useState, useEffect} from 'react';
function SalesForm () {
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [salespersonList, setSalespersonList] = useState([]);
    const [customer, setCustomer] = useState('');
    const [customerList, setCustomerList] = useState([]);
    const [price, setPrice] = useState('');
    const [availableCars, setAvailableCars] = useState([]);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;
        data.price = price;

        const salesUrl = 'http://localhost:8090/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(salesUrl, fetchConfig);
            if (response.ok) {
                setAutomobile('');
                setSalesperson('');
                setCustomer('');
                setPrice('');
                fetchAvailable();
            }
        }

        const fetchAvailable = async () => {
            const url = "http://localhost:8090/available/"

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setAvailableCars(data.Available);

            }
        }


        const fetchSalesperson = async () => {
            const url = "http://localhost:8090/salespeople/"

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setSalespersonList(data.Salespeople);
            }
        }

        const fetchCustomer = async () => {
            const url = "http://localhost:8090/customers/"

            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setCustomerList(data.Customer);
            }
        }

        const handleAutomobileChange = (event) => {
            const value = event.target.value;
            setAutomobile(value);
        }

        const handleSalespersonChange = (event) => {
            const value = event.target.value;
            setSalesperson(value);
        }

        const handleCustomerChange = (event) => {
            const value = event.target.value;
            setCustomer(value);
        }

        const handlePriceChange = (event) => {
            const value = event.target.value;
            setPrice(value);
        }

        useEffect(() => {
            fetchCustomer();
            fetchAvailable();
            fetchSalesperson();
        }, []);





  return (
    <>
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a New Sale</h1>
          <form onSubmit={handleSubmit} id="create-sale-form">
            <div className=" mb-3">
              <select value={automobile} onChange={handleAutomobileChange} id="automobile" className="form-select" required name="automobile">
              <option value="">Choose an Automobile</option>
              {availableCars.map(car => {
                return (
                    <option key={car["id, vin"][1]} value={car["id, vin"][1]}>
                        {car["id, vin"][1]}
                    </option>
                );
              })}
              </select>
            </div>
            <div className="mb-3">
              <select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" className="form-select" name="salesperson">
              <option>Salespeople</option>
              {salespersonList.map(person => {
                return (
                    <option value = {person.name} key = {person.name}>
                        {person.name}
                    </option>
                );
              })}
              </select>
            </div>
            <div className="mb-3">
              <select value={customer} onChange={handleCustomerChange} required id="customer" className="form-select" name="customer">
              <option>Customer</option>
              {customerList.map(customer => {
                return (
                    <option value = {customer.name} key = {customer.name}>
                        {customer.name}
                    </option>
                )
              })}
              </select>
            </div>
            <div className="form-floating mb-3">
                  <input value={price} onChange={handlePriceChange} placeholder="price" required type="number" name="price" id="price" className="form-control" />
                  <label htmlFor="price">Price</label>
                </div>
            <button className="btn btn-primary">Submit</button>
          </form>
        </div>
      </div>
    </div>
    </>
  );

}
export default SalesForm;
