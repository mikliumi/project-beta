import React, {useState, useEffect} from 'react';

function AutomobileForm () {
    const [models, setModels] = useState([])
    const [formData, setFormData] = useState({
        vin: '',
        color: '',
        year: '',
        model_id: ''
    })

    const getData = async () => {
        const url = `http://localhost:8100/api/models/`;
        const response = await fetch(url);

        if(response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    useEffect(()=> {
        getData();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const automobilesUrl = `http://localhost:8100/api/automobiles/`;

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(automobilesUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                vin: '',
                color: '',
                year: '',
                model_id: ''
            });
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputId = e.target.id;
        setFormData({
            ...formData,
            [inputId]: value
        });
    }

    return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Automobile</h1>
              <form onSubmit={handleSubmit} id="create-automobile-form">
                <div className="form-floating mb-3">
                  <input value={formData.vin} onChange={handleFormChange} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                  <label htmlFor="vin">VIN</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={formData.color} onChange={handleFormChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                  <label htmlFor="color">color</label>
                </div>
                <div className="form-floating mb-3">
                  <input value={formData.year} onChange={handleFormChange} placeholder="year" required type="text" name="year" id="year" className="form-control" />
                  <label htmlFor="year">year</label>
                </div>
                <div className="mb-3">
                  <select value={formData.model_id} onChange={handleFormChange} required name="model_id" id="model_id" className="form-select">
                    <option>Choose a model</option>
                    {models.map(model => {
                      return (
                        <option key={model.id} value={model.id}>
                          {model.name}
                        </option>
                      );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
    );
 }

export default AutomobileForm;
